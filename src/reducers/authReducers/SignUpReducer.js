import { authTypes } from '@/constants';
const { SignUp, NotifyType } = authTypes;

const initialState = {
    isFetching: false,
    message: null,
    isError: false,
    errorMessage: null,
}

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case SignUp.SIGN_UP_REQUEST:
        case NotifyType.CLEAR_REQUEST:
            return {
                ...state,
                isFetching: true,
                message: null,
                isError: false,
                errorMessage: null,
            }
        case SignUp.SIGN_UP_SUCCESS:
        case NotifyType.CLEAR_SUCCESS:
            return {
                ...state,
                isFetching: false,
                message: payload.message,
                isError: false,
                errorMessage: null,
            }
        case SignUp.SIGN_UP_FAILURE:
        case NotifyType.CLEAR_FAILURE:
            return {
                ...state,
                isFetching: false,
                message: null,
                isError: true,
                errorMessage: payload.message,
            }
        default:
            return state
    }
}
