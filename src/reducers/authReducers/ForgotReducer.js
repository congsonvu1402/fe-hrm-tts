import { authTypes } from "@/constants";

const { Forgot, NotifyType } = authTypes

const INITIAL_STATE = {
  isFetching: false,
  message: null,
  isError: false,
  errorMessage: null,
};

export default function ForgotReducer(
  state = INITIAL_STATE,
  { type, payload }
) {
  switch (type) {
    case Forgot.FORGOT_PASSWORD_REQUEST:
    case NotifyType.CLEAR_REQUEST:
      return {
        ...state,
        isFetching: true,
        message: null,
        isError: false,
        errorMessage: null,
      };
    case Forgot.FORGOT_PASSWORD_SUCCESS:
    case NotifyType.CLEAR_SUCCESS:
      return {
        ...state,
        isFetching: false,
        message: payload.message,
        isError: false,
        errorMessage: null,
      };
    case Forgot.FORGOT_PASSWORD_FAILURE:
    case NotifyType.CLEAR_FAILURE:
      return {
        ...state,
        isFetching: false,
        message: null,
        isError: true,
        errorMessage: payload.message,
      };
    default:
      return state;
  }
}
