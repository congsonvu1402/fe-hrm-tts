import { authTypes } from "@/constants";

const { signInTypes, CheckTokenTime, NotifyType, SignOutType } = authTypes;

const INITIAL_STATE = {
  userToken: null,
  isFetching: false,
  message: null,
  isError: false,
  errorMessage: null,
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case signInTypes.SIGN_IN_REQUEST:
    case NotifyType.CLEAR_REQUEST:
    case CheckTokenTime.TOKEN_TIME_REQUEST:
    case SignOutType.SIGN_OUT_REQUEST:
      return {
        ...state,
        isFetching: true,
        userToken: null,
        message: null,
        isError: false,
        errorMessage: null,
      };
    case signInTypes.SIGN_IN_SUCCESS:
    case NotifyType.CLEAR_SUCCESS:
    case CheckTokenTime.TOKEN_TIME_SUCCESS:
    case SignOutType.SIGN_OUT_SUCCESS:
      return {
        ...state,
        isFetching: false,
        message: payload.message,
        userToken: payload.userToken,
        isError: false,
        errorMessage: null,
      };
    case signInTypes.SIGN_IN_FAILURE:
    case NotifyType.CLEAR_FAILURE:
    case CheckTokenTime.TOKEN_TIME_FAILURE:
    case SignOutType.SIGN_OUT_FAILURE:
      return {
        ...state,
        isFetching: false,
        message: null,
        isError: true,
        errorMessage: payload.message,
        userToken: null,
      };
    default:
      return state;
  }
}
