import SignInReducer from './SignInReducer';
import SignUpReducer from './SignUpReducer';
import ForgotReducer from './ForgotReducer'
import ChangePassReducer from './ChangePassReducer';
import OtpReducer from './OtpReudcer';

export {
    ForgotReducer,
    SignUpReducer,
    SignInReducer,
    ChangePassReducer,
    OtpReducer,
}