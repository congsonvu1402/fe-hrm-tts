import { authTypes } from '@/constants';
const { ChangePass, NotifyType } = authTypes;

const initialState = {
    isFetching: false,
    message: null,
    isError: false,
    errorMessage: null,
}

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case ChangePass.CHANGE_PASSWORD_REQUEST:
        case NotifyType.CLEAR_REQUEST:
            return {
                ...state,
                isFetching: true,
                message: null,
                isError: false,
                errorMessage: null,
            }
        case NotifyType.CLEAR_REQUEST:
            return {
                ...state,
                isFetching: true,
                message: null,
                isError: false,
                errorMessage: null,
            }
        case ChangePass.CHANGE_PASSWORD_SUCCESS:
            return {
                ...state,
                isFetching: false,
                message: payload.message,
                isError: false,
                errorMessage: null,
            }
        case NotifyType.CLEAR_SUCCESS:
            return {
                ...state,
                isFetching: false,
                message: payload.message,
                isError: false,
                errorMessage: null,
            }
        case ChangePass.CHANGE_PASSWORD_FAILURE:
        case NotifyType.CLEAR_FAILURE:
            return {
                ...state,
                isFetching: false,
                message: null,
                isError: true,
                errorMessage: payload.message,
            }
        default:
            return state
    }
}
