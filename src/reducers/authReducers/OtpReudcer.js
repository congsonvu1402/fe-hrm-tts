import { authTypes } from '@/constants'

const { OtpType, NotifyType } = authTypes

const INITIAL_STATE = {
    isFetching: false,
    message: null,
    isError: false,
    errorMessage: null,
    userToken: null,
}

export default function OtpReducer(state = INITIAL_STATE, { type, payload }) {
    switch (type) {
        case NotifyType.CLEAR_REQUEST:
        case OtpType.OTP_REQUEST:
            return {
                ...state,
                isFetching: true,
                isError: false,
                errorMessage: null,
                message: null
            }
        case OtpType.OTP_SUCCESS:
        case NotifyType.CLEAR_SUCCESS:
            return {
                ...state,
                isFetching: false,
                message: payload.message,
                isError: false,
                errorMessage: null,
            }
        case OtpType.OTP_FAILURE:
        case NotifyType.CLEAR_FAILURE:
            return {
                ...state,
                isFetching: true,
                message: null,
                errorMessage: payload.message,
                isError: true,
            }
        default:
            return state
    }
}