import { combineReducers } from 'redux'
import * as authReducer from './authReducers'
// import * as  authReducer from './authReducers'
const {ForgotReducer, SignUpReducer, SignInReducer, ChangePassReducer,OtpReducer} = authReducer


export default combineReducers({
    forgot: ForgotReducer,
    signUp: SignUpReducer,
    signIn: SignInReducer,
    changePassword: ChangePassReducer,
    otp: OtpReducer,
})