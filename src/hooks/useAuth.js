import { useDispatch, useSelector } from 'react-redux'
import { authActions } from '@/actions'

export const useAuth = () => {
    const dispatch = useDispatch()
    const SignInState = useSelector(state => state.signIn)
    const SignUpState = useSelector(state => state.signUp)
    const ForgotState = useSelector(state => state.forgot)
    const OtpState = useSelector(state=>state.otp)
    const ChangePasswordState = useSelector(state => state.changePassword)

    const handleSignIn = ({ email, password }) => {
        dispatch(authActions.SignInAction.signInRequest({ email, password }))
    }
    const handleSignUp = ({ fullName, email, password, phone, introCode }) => {
        dispatch(authActions.SignUpAction.signUpRequest({ fullName, email, password, phone, introCode }))
    }
    const handleForgot = ({ email }) => {
        dispatch(authActions.ForgotPassAction.forgotPasswordRequest({ email }))
    }
    const handleChangePassword = ({ password }) => {
        dispatch(authActions.ChangePassAction.ChangePassRequest({ password }))
    }
    const handleNotify = ({ message, errorMessage }) => {
        dispatch(authActions.NotifyAction.NotifyRequest({ message, errorMessage }))
    }
    const handleCheckTokenTime = ({ token }) => {
        dispatch(authActions.CheckTokenTimeAction.CheckTokenTimeRequest({ token }))
    }
    const handleSignOut = () => {
        dispatch(authActions.SignOutAction.SignOutRequest())
    }
    const handleOtp = (payload) => {
        dispatch(authActions.OtpAction.otpRequest(payload))
    }

    return {
        ForgotState,
        SignUpState,
        SignInState,
        ChangePasswordState,
        OtpState,
        handleSignIn,
        handleSignUp,
        handleForgot,
        handleChangePassword,
        handleNotify,
        handleCheckTokenTime,
        handleSignOut,
        handleOtp,
    }
}