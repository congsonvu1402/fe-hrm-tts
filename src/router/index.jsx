import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import screens from '@/screens'
import { useAuth } from '@/hooks';
const Stack = createNativeStackNavigator()

export default function Screens() {
    const { SignUpState } = useAuth()

    // const token = SignUpState.token;
    const token = null;

    return (
        <NavigationContainer>
            <Stack.Navigator
                initialRouteName='SignIn'
                screenOptions={{
                    headerShown: false
                }}
            >
                {
                    token !== null && token !== undefined
                        ? (
                            <>
                                <Stack.Screen name="Home" component={screens.Home} />
                            </>
                        ) : (
                            <>
                                <Stack.Screen name="SignIn" component={screens.authScreens.SignIn} />
                                <Stack.Screen name="SignUp" component={screens.authScreens.SignUp} />
                                <Stack.Screen name="ForgotPassword" component={screens.authScreens.ForgotPassword} />
                                <Stack.Screen name="ChangePassword" component={screens.authScreens.ChangePassword} />
                                <Stack.Screen name="Otp" component={screens.authScreens.Otp} />
                            </>
                        )
                }
            </Stack.Navigator>
        </NavigationContainer>
    )
}



