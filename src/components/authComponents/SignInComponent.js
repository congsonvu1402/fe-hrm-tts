import React, { useState, useEffect } from 'react'
import { View, Text, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { useAuth } from '@/hooks';
import * as Common from '../Common';
import { TextInput } from "react-native-paper";
import { verifyEmail, verifyPassword } from '@/utils/validators';
import { SignInStyle } from '../../styleComponent/SignInStyle';
import * as utils from '@/utils'
const { getStore } = utils.Storage;

export default function SignInComponents(props) {
  const {
    SignInState,
    handleNotify,
    handleCheckTokenTime,
    handleSignIn
  } = useAuth();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false)
  const [visible, setVisible] = useState(false)
  const { navigation } = props;
  const handlePress = () => {
    if (email && password && verifyEmail(email) && verifyPassword(password)) {
    handleSignIn({ email, password })
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 3000);
    } else {
      setVisible(true)
    }
  }
  const getDataSecureStore = async () => {
    const userEmail = await getStore("Email")
    setEmail(userEmail)
    const userToken = await getStore("userToken")
    const token = { token: userToken }
    handleCheckTokenTime(token)
  }
  useEffect(() => {
    getDataSecureStore()
    return () => {
      setEmail('')
    }
  }, [])
  return (
    <KeyboardAvoidingView>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={SignInStyle.screen}>
          <LinearGradient colors={Common.Color.linear}
            style={SignInStyle.background}
          >
            <>
              <Common.LoadingModal
                loader={loading}
              />
              <Common.ModalNotification.SuccessModal
                visible={SignInState.message ? true : false && loading === false}
                title={SignInState.message}
                onPress={() => {
                  handleNotify({
                    message: SignInState.message
                  })
                }}
              />
              <Common.ModalNotification.FailModal
                visible={(SignInState.errorMessage ? true : false && loading === false)}
                title={SignInState.errorMessage}
                onPress={() => {
                  handleNotify({
                    errorMessage: SignInState.errorMessage
                  })
                }} />
              <Common.ModalNotification.FailModal
                visible={visible}
                title={'Ban chua nhap du cac truong'}
                onPress={() => {
                  setVisible(false)
                }} />
            </>
            <View style={SignInStyle.header}>
              <Text style={[SignInStyle.bigTitle, SignInStyle.font]}>
                {Common.Title.SIGN_IN}
              </Text>
              <Text style={[SignInStyle.smallTitle, SignInStyle.font]}>
                {Common.Title.PLEASE_SIGN_IN}
              </Text>
            </View>
            <View style={SignInStyle.container}>
              <View style={SignInStyle.container_input}>
                <View style={[SignInStyle.valid, SignInStyle.input]}>
                  <Common.InputCommon
                    left={<TextInput.Icon name="email-outline" />}
                    placeholder={Common.Title.EMAIL}
                    value={email}
                    onChangeText={setEmail} />
                  <Text style={SignInStyle.text}>{verifyEmail(email) === true ? '' : 'Email isValid'}</Text>
                </View>
                <View style={[SignInStyle.valid, SignInStyle.input, SignInStyle.marginTop_5]}>
                  <Common.InputCommon
                    left={<TextInput.Icon name="lock-outline" />}
                    placeholder={Common.Title.PASSWORD}
                    secureTextEntry={true}
                    value={password}
                    onChangeText={setPassword} />
                  <Text style={SignInStyle.text}>{verifyPassword(password) === true ? '' : 'Password isValid'}</Text>
                </View>
              </View>
              <View style={SignInStyle.btn}>
                <Common.ButtonCommon
                  backgroundColor={Common.Color.black}
                  height={'100%'}
                  title={Common.Title.SIGN_IN_BUTTON}
                  onPress={() => handlePress()}
                />
              </View>
            </View>
            <View style={SignInStyle.footer}>
              <Text style={[SignInStyle.footerText]} onPress={() => navigation.navigate('SignUp')}>Đăng ký tài khoản </Text>
              <Text style={SignInStyle.verticalLine}> | </Text>
              <Text style={[SignInStyle.footerText]} onPress={() => navigation.navigate('ForgotPassword')}> Quên mật khẩu </Text>
            </View>
          </LinearGradient>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  )
}

