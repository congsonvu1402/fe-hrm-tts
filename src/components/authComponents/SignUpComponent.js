import { View, Text, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard } from "react-native";
import React, { useState, useEffect } from 'react';
import { useAuth } from '@/hooks';
import { LinearGradient } from 'expo-linear-gradient';
import { Validators } from '@/utils';
import * as Common from '../Common';
import { style } from "@/constants";
import { SignUpStyle } from '../../styleComponent/SignUpStyle';
import { TextInput } from "react-native-paper";

export default function SignUpComponent(props) {
    const [fullName, setFullName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [phone, setPhone] = useState('');
    const [introCode, setIntroCode] = useState('');
    const [loading, setLoading] = useState(false);
    const [visible, setVisible] = useState(false);
    const { navigation } = props;
    const {
        SignUpState,
        handleSignUp,
        handleNotify,
    } = useAuth();
    const verifyEmail = Validators.verifyEmail(email)
    const verifyPassword = Validators.verifyPassword(password)
    const verifyPhone = Validators.verifyPhone(phone)
    const handlePress = () => {
        if (fullName && email && password && confirmPassword && phone && introCode && verifyPhone && verifyEmail && verifyPassword) {
            handleSignUp({ fullName, email, password, phone, introCode })
            setLoading(true);
            setTimeout(() => {
                setLoading(false);
            }, 3000);
        } else {
            setVisible(true)
        }
    }
    return (
        <KeyboardAvoidingView>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={SignUpStyle.screen}>
                    <LinearGradient colors={Common.Color.linear}
                        style={SignUpStyle.background}
                    >
                        <>
                            <Common.LoadingModal
                                loader={loading}
                            />
                            <Common.ModalNotification.SuccessModal
                                visible={SignUpState.message ? true : false && loading === false}
                                title={SignUpState.message}
                                onPress={() => {
                                    navigation.navigate('SignIn')
                                    handleNotify({
                                        message: SignUpState.message
                                    })
                                }}
                            />
                            <Common.ModalNotification.FailModal
                                visible={(SignUpState.errorMessage ? true : false && loading === false)}
                                title={SignUpState.errorMessage}
                                onPress={() => {
                                    handleNotify({
                                        errorMessage: SignUpState.errorMessage
                                    })
                                }} />
                            <Common.ModalNotification.FailModal
                                visible={visible}
                                title={'Ban chua nhap du cac truong'}
                                onPress={() => {
                                    setVisible(false)
                                }} />
                        </>
                        <View style={SignUpStyle.header}>
                            <Common.BackButton
                                onPress={() => { navigation.goBack() }}
                            />
                            <Text style={[SignUpStyle.title, SignUpStyle.font]}>
                                {Common.Title.CREATE_ACC}
                            </Text>
                        </View>
                        <View style={SignUpStyle.container}>
                            <View style={[SignUpStyle.valid, SignUpStyle.input]}>
                                <Common.InputCommon
                                    left={<TextInput.Icon name="account-outline" />}
                                    placeholder={Common.Title.NAME}
                                    value={fullName}
                                    onChangeText={setFullName} />
                            </View>
                            <View style={[SignUpStyle.valid, SignUpStyle.input]}>
                                <Common.InputCommon
                                    left={<TextInput.Icon name="email-outline" />}
                                    placeholder={Common.Title.EMAIL}
                                    value={email}
                                    onChangeText={setEmail} />
                                <Text style={SignUpStyle.text}>{verifyEmail === true ? '' : 'Email isValid'}</Text>
                            </View>
                            <View style={[SignUpStyle.valid, SignUpStyle.input]}>
                                <Common.InputCommon
                                    left={<TextInput.Icon name="lock-outline" />}
                                    placeholder={Common.Title.PASSWORD}
                                    secureTextEntry={true}
                                    value={password}
                                    onChangeText={setPassword} />
                                <Text style={SignUpStyle.text}>{verifyPassword === true ? '' : 'Password isValid'}</Text>
                            </View>
                            <View style={[SignUpStyle.valid, SignUpStyle.input]}>
                                <Common.InputCommon
                                    left={<TextInput.Icon name="lock-outline" />}
                                    placeholder={Common.Title.CONFIRM_PASS}
                                    secureTextEntry={true}
                                    value={confirmPassword}
                                    onChangeText={setConfirmPassword} />
                                <Text style={SignUpStyle.text}>{(password !== confirmPassword && confirmPassword !== '') ? 'Password do not match' : ''}</Text>
                            </View>
                            <View style={[SignUpStyle.valid, SignUpStyle.input]}>
                                <Common.InputCommon
                                    left={<TextInput.Icon name="phone" />}
                                    placeholder={Common.Title.SDT}
                                    value={phone}
                                    keyboardType={'numeric'}
                                    onChangeText={setPhone} />
                                <Text style={SignUpStyle.text}>{verifyPhone === true ? '' : 'Phone number isValid'}</Text>
                            </View>
                            <View style={[SignUpStyle.valid, SignUpStyle.input]}>
                                <Common.InputCommon
                                    left={<TextInput.Icon name="qrcode" />}
                                    placeholder={Common.Title.CODE}
                                    value={introCode}
                                    onChangeText={setIntroCode} />
                            </View>
                        </View>
                        <View style={SignUpStyle.footer}>
                            <View style={SignUpStyle.btn}>
                                <Common.ButtonCommon
                                    backgroundColor={Common.Color.black}
                                    height={'100%'}
                                    title={Common.Title.SIGN_UP_BUTTON}
                                    onPress={() => handlePress()}
                                />
                            </View>
                        </View>
                    </LinearGradient>
                </View>
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
    )
}
