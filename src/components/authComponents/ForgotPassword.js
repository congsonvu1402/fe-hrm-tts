import React, { useState } from "react";
import { LinearGradient } from "expo-linear-gradient";
import {
  View,
  Text,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
  ScrollView,
  SafeAreaView,
} from "react-native";
import { Common } from "../index";
import { style } from "@/constants";
import { ForgotPass } from "../../styleComponent/StyleForgotPass";
import { useAuth } from "@/hooks";
import { TextInput } from "react-native-paper";
import { verifyEmail } from "@/utils/validators";

const ForgotPassword = (props) => {
  const { navigation } = props;
  const [email, setEmail] = useState("");
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = useState(false);
  const { ForgotState, handleForgot, handleNotify } = useAuth();
  const handlePress = () => {
    if (email && verifyEmail(email)) {
      handleForgot({ email });
      setLoading(true);
      setTimeout(() => {
        setLoading(false);
        Platform.OS === "ios" ? 3000 : 0;
      }, 3000);
    } else {
      setVisible(true);
    }
  };

  return (
  
        <KeyboardAvoidingView>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View>
              <LinearGradient
                colors={Common.Color.linear}
                style={ForgotPass.background}
              >
                <Common.BackButton
                  onPress={() => navigation.goBack()}
                ></Common.BackButton>
                <Text style={ForgotPass.title1}>
                  {Common.Title.FORGOT_PASS}
                </Text>
                <View style={ForgotPass.wrapText}>
                  <Text style={ForgotPass.title2}>
                    {Common.Title.TITTLE_FORGOT_PASS1}
                  </Text>
                  <Text style={ForgotPass.title2}>
                    {Common.Title.TITTLE_FORGOT_PASS2}
                  </Text>
                </View>
                
                <View style={ForgotPass.wrapContent}>
                  <Common.InputCommon
                    placeholder={Common.Title.EMAIL}
                    left={<TextInput.Icon name="email-outline" />}
                    width={style.INPUT_AUTHEN.width}
                    height={style.INPUT_AUTHEN.height}
                    heightInput={style.HEIGHT_INPUT.height}
                    value={email}
                    onChangeText={setEmail}
                  ></Common.InputCommon>
                  <Text style={{ color: "white" }}>
                    {verifyEmail(email) ? "" : "Sai định dạng email"}
                  </Text>

                  <View style={ForgotPass.button}>
                    <Common.ButtonCommon
                      backgroundColor={Common.Color.black}
                      width={style.BTN_CONFIRM_AUTHEN.width}
                      height={style.BTN_CONFIRM_AUTHEN.height}
                      title={Common.Title.CONFIRM}
                      onPress={() => handlePress()}
                    ></Common.ButtonCommon>
                  </View>
                </View>

                <Common.LoadingModal
                  loader={loading}
                  animate={loading}
                ></Common.LoadingModal>

                <Common.ModalNotification.SuccessModal
                  visible={ForgotState.message ? true : false && loading}
                  title={ForgotState.message}
                  onPress={() => {
                    handleNotify({ message: ForgotState.message });
                    navigation.navigate("SignIn");
                  }}
                ></Common.ModalNotification.SuccessModal>
                <Common.ModalNotification.FailModal
                  visible={ForgotState.errorMessage ? true : false && loading}
                  title={ForgotState.errorMessage}
                  onPress={() => {
                    handleNotify({ errorMessage: ForgotState.errorMessage });
                  }}
                ></Common.ModalNotification.FailModal>
              </LinearGradient>
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
  );
};

export default ForgotPassword;
