//Component
import React, { useState, useEffect, useRef } from "react";
import { style } from "@/constants";
import { View, Text, TextInput, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Common } from "..";
import { OtpStyle } from "../../styleComponent/OtpStyle";
import { useAuth } from '@/hooks';

const OtpComponent = ({ navigation }) => {
  const [loading, setLoading] = useState(false)
  const [countdown, setCountdown] = useState(5)
  const [enableResend, setEnableResend] = useState(false);
  const firstTouch = useRef();
  const secondTouch = useRef();
  const thirdTouch = useRef();
  const fourthTouch = useRef();
  const fifthTouch = useRef();
  const sixthTouch = useRef();
  const [otp, setOtp] = useState('');
  const lengthOtp = Object.keys(otp).length
  var clockCall = null
  const [visible, setVisible] = useState(false);

  const deleteHandle = (nativeEvent, numb) => {
    if (nativeEvent.key === 'Backspace') {
      switch (numb) {
        case 'two': {
          !(otp.two) ? firstTouch.current.focus() && otp.one === ''
            : secondTouch.current.focus()
          break
        }
        case 'three': {
          !(otp.three) ? secondTouch.current.focus() && otp.two === ''
            : thirdTouch.current.focus()
          break
        }
        case 'four': {
          !(otp.four) ? thirdTouch.current.focus() && otp.three === ''
            : fourthTouch.current.focus()
          break
        }
        case 'five': {
          !(otp.five) ? fourthTouch.current.focus() && otp.four === ''
            : fifthTouch.current.focus()
          break
        }
        case 'six': {
          !(otp.six) ? fifthTouch.current.focus() && otp.five === ''
            : sixthTouch.current.focus()
          break
        }
        default: {
          break
        }
      }
    }
  }
  const {
    OtpState,
    handleOtp,
    handleNotify
  } = useAuth()

  useEffect(() => {
    clockCall = setInterval(() => {
      decrementClock();
    }, 1000);
    return () => {
      clearInterval(clockCall);
    };
  });
  const decrementClock = () => {
    if (countdown === 0 || countdown === 'Gửi lại') {
      clearInterval(clockCall)
      setCountdown('Gửi lại')
    } else {
      setCountdown(countdown - 1)
    }
  }
  return (
    <KeyboardAvoidingView>
      <View>
        <LinearGradient
          colors={['#FF971D', '#EE2C2C']}
          style={OtpStyle.body}
        >
          <>
            <Common.LoadingModal
              loader={loading}
            />
            <Common.ModalNotification.SuccessModal
              visible={OtpState.message ? true : false && loading === false}
              title={OtpState.message}
              onPress={() => {
                handleNotify({
                  message: OtpState.message
                })
                navigation.navigate('SignIn')
              }}
            />
            <Common.ModalNotification.FailModal
              visible={(OtpState.errorMessage ? true : false && loading === false)}
              title={OtpState.errorMessage}
              onPress={() => {
                handleNotify({
                  errorMessage: OtpState.errorMessage
                })
              }} />
            <Common.ModalNotification.FailModal
              visible={visible}
              title={'Ban chua nhap du cac truong'}
              onPress={() => {
                setVisible(false)
              }} />
          </>

          <Common.LoadingModal
            loader={loading}
          />
          <View style={OtpStyle.title}>
            <Common.BackButton
              onPress={() => { navigation.goBack() }}
            />
            <Text style={OtpStyle.textTitle}>
              {Common.Title.OTP}
            </Text>
          </View>
          <View style={OtpStyle.inputOtps}>
            <TextInput
              style={OtpStyle.otpBox}
              maxLength={1}
              keyboardType="numeric"
              ref={firstTouch}
              value = {otp.one}
              onChangeText={(text) => {
                setOtp({ ...otp, one: text });
                text && secondTouch.current.focus();
              }}
            />
            <TextInput
              style={OtpStyle.otpBox}
              maxLength={1}
              keyboardType="numeric"
              ref={secondTouch}
              value = {otp.two}
              onChangeText={(text) => {
                setOtp({ ...otp, two: text });
                text
                  ? thirdTouch.current.focus()
                  : firstTouch.current.focus();
              }}
              onKeyPress={({ nativeEvent }) => {
                deleteHandle(nativeEvent, 'two')
              }}
            />
            <TextInput
              style={OtpStyle.otpBox}
              maxLength={1}
              keyboardType="numeric"
              ref={thirdTouch}
              value = {otp.three}
              onChangeText={(text) => {
                setOtp({ ...otp, three: text });
                text
                  ? fourthTouch.current.focus()
                  : secondTouch.current.focus();
              }}
              onKeyPress={({ nativeEvent }) => {
                deleteHandle(nativeEvent, 'three')
              }}
            />
            <TextInput
              style={OtpStyle.otpBox}
              maxLength={1}
              keyboardType="numeric"
              ref={fourthTouch}
              value = {otp.four}
              onChangeText={(text) => {
                setOtp({ ...otp, four: text });
                text
                  ? fifthTouch.current.focus()
                  : thirdTouch.current.focus();
              }}
              onKeyPress={({ nativeEvent }) => {
                deleteHandle(nativeEvent, 'four')
              }}
            />
            <TextInput
              style={OtpStyle.otpBox}
              maxLength={1}
              keyboardType="numeric"
              ref={fifthTouch}
              value = {otp.five}
              onChangeText={(text) => {
                setOtp({ ...otp, five: text });
                text
                  ? sixthTouch.current.focus()
                  : fourthTouch.current.focus();
              }}
              onKeyPress={({ nativeEvent }) => {
                deleteHandle(nativeEvent, 'five')
              }}
            />
            <TextInput
              style={OtpStyle.otpBox}
              maxLength={1}
              keyboardType="numeric"
              ref={sixthTouch}
              value = {otp.six}
              onChangeText={(text) => {
                setOtp({ ...otp, six: text });
                !text && fifthTouch.current.focus();
              }}
              onKeyPress={({ nativeEvent }) => {
                deleteHandle(nativeEvent, 'six')
              }}
            />
          </View>
          <View style={OtpStyle.setTime}>
            <Text style={OtpStyle.setTimeText}>Mã OTP sẽ hết hạn sau <Text style={OtpStyle.textSend} onPress={() => { setCountdown(5) }}>{countdown}</Text> giây</Text>
          </View>
          <View style={OtpStyle.confirm}>
            <Common.ButtonCommon
              backgroundColor={Common.Color.black}
              width={style.BTN_CONFIRM.width}
              height={style.BTN_CONFIRM.height}
              title={Common.Title.CONFIRM}
              onPress={() => {
                if (otp.one && otp.two && otp.three && otp.four && otp.five && otp.six) {
                  setLoading(true)
                  setTimeout(() => {
                    setLoading(false);
                    handleOtp(otp);
                }, 1000);
                } else {
                  setVisible(true)
                }
              }}
            ></Common.ButtonCommon>
          </View>
        </LinearGradient>
      </View>
    </KeyboardAvoidingView>

  );
};
export default OtpComponent;

