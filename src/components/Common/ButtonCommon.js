import React from "react";
import { Pressable, Text, StyleSheet } from "react-native";

export const ButtonCommon = (props) => {
  const { title, onPress, backgroundColor, width, height, color, disabled } = props;
  return (
    <Pressable
      style={styles.buttonStyle}
      backgroundColor={backgroundColor}
      onPress={onPress}
      width={width}
      height={height}
      disabled={disabled}
    >
      <Text color={color} style={styles.textStyle}>
        {title}
      </Text>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  textStyle: {
    fontFamily: "Roboto",
    fontSize: 18,
    fontWeight: "400",
    color: "white",
  },
  buttonStyle: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
    backgroundColor: "black",
  },
});
