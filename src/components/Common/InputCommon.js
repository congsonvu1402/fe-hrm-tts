import React from "react";
import { StyleSheet, View } from "react-native";
import { TextInput } from "react-native-paper";

export const InputCommon = (props) => {
  const { onChangeText, value, placeholder, secureTextEntry, heightInput, left, width, height, keyboardType } = props;
  return (
    <View style={styles.wrapInput} width={width} height={height}>
      <TextInput
        style={styles.input}
        height={heightInput}
        label={placeholder}
        secureTextEntry={secureTextEntry}
        keyboardType={keyboardType}
        left={left}
        mode="flat"
        theme={{colors: {text: 'black', primary: ['#ABABAB','transparent']}}}
        value={value}
        onChangeText={onChangeText}
        keyboardType={keyboardType}
      ></TextInput>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapInput: {
    borderRadius: 10,
    overflow: "hidden",
    borderColor: "white",
    justifyContent: "center",
  },
  input: {
    borderRadius: 0,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    overflow: "hidden",
    backgroundColor: "#fff",
    fontSize: 16
  },
});
