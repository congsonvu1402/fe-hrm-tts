import { createAction } from "@reduxjs/toolkit";
import { authTypes } from "../../constants/index"

export const SignInAction = {
    signInRequest: createAction(authTypes.signInTypes.SIGN_IN_REQUEST),
    signInSuccess: createAction(authTypes.signInTypes.SIGN_IN_SUCCESS),
    signInFailure: createAction(authTypes.signInTypes.SIGN_IN_FAILURE),
}