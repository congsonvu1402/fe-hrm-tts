import { createAction } from "@reduxjs/toolkit";
import { authTypes } from '@/constants';

export const NotifyAction = {
    NotifyRequest: createAction(authTypes.NotifyType.CLEAR_REQUEST),
    NotifySuccess: createAction(authTypes.NotifyType.CLEAR_SUCCESS),
    NotifyFailure: createAction(authTypes.NotifyType.CLEAR_FAILURE)
}