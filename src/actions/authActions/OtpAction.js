import { createAction } from "@reduxjs/toolkit";
import { authTypes } from "../../constants/index"

export const OtpAction = {
    otpRequest: createAction(authTypes.OtpType.OTP_REQUEST),
    otpSuccess: createAction(authTypes.OtpType.OTP_SUCCESS),
    otpFailure: createAction(authTypes.OtpType.OTP_FAILURE),
}