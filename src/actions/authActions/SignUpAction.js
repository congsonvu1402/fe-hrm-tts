import { createAction } from "@reduxjs/toolkit";
import { authTypes } from '@/constants';

export const SignUpAction = {
    signUpRequest: createAction(authTypes.SignUp.SIGN_UP_REQUEST),
    signUpSuccess: createAction(authTypes.SignUp.SIGN_UP_SUCCESS),
    signUpFailure: createAction(authTypes.SignUp.SIGN_UP_FAILURE)
}