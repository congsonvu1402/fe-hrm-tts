import { createAction } from "@reduxjs/toolkit";
import { authTypes } from '@/constants';

export const CheckTokenTimeAction = {
    CheckTokenTimeRequest: createAction(authTypes.CheckTokenTime.TOKEN_TIME_REQUEST),
    CheckTokenTimeSuccess: createAction(authTypes.CheckTokenTime.TOKEN_TIME_SUCCESS),
    CheckTokenTimeFailure: createAction(authTypes.CheckTokenTime.TOKEN_TIME_FAILURE)
}