import { createAction } from "@reduxjs/toolkit";
import { authTypes } from '@/constants'

export const ForgotPassAction = {
    forgotPasswordRequest: createAction(authTypes.Forgot.FORGOT_PASSWORD_REQUEST),
    forgotPasswordSuccess: createAction(authTypes.Forgot.FORGOT_PASSWORD_SUCCESS),
    forgotPasswordFailure: createAction(authTypes.Forgot.FORGOT_PASSWORD_FAILURE)
}