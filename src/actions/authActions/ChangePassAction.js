import { createAction } from "@reduxjs/toolkit";
import { authTypes } from '@/constants';

export const ChangePassAction = {
    ChangePassRequest: createAction(authTypes.ChangePass.CHANGE_PASSWORD_REQUEST),
    ChangePassSuccess: createAction(authTypes.ChangePass.CHANGE_PASSWORD_SUCCESS),
    ChangePassFailure: createAction(authTypes.ChangePass.CHANGE_PASSWORD_FAILURE)
}