import { createAction } from "@reduxjs/toolkit";
import { authTypes } from '@/constants'

export const Confirm = {
    forgotPasswordRequest: createAction(authTypes.Confirm.CONFIRM_REQUEST),
    forgotPasswordSuccess: createAction(authTypes.Confirm.CONFIRM_SUCCESS),
    forgotPasswordFailure: createAction(authTypes.Confirm.CONFIRM_FAILURE)
}