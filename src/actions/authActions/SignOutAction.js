import { createAction } from "@reduxjs/toolkit";
import { authTypes } from "../../constants/index"

export const SignOutAction = {
    SignOutRequest: createAction(authTypes.SignOutType.SIGN_OUT_REQUEST),
    SignOutSuccess: createAction(authTypes.SignOutType.SIGN_OUT_SUCCESS),
    SignOutFailure: createAction(authTypes.SignOutType.SIGN_OUT_FAILURE),
}