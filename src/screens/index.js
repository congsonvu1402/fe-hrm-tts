import * as authScreens from './authScreens'
import Home from './HomeScreen'


const screens = {
    authScreens,
    Home
}

export default screens;