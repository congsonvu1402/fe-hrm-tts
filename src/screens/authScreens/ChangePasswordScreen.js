import React from 'react';
import ChangePass from '../../components/authComponents/ChangePassComponent'

export default function ChangePasswordScreen(props) {
    return (
        <ChangePass {...props} />
    )
}