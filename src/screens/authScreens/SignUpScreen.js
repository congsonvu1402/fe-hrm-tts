import React from 'react';
import SignUp from '../../components/authComponents/SignUpComponent'

export default function SignUpScreen(props) {
    return (
        <SignUp {...props} />
    )
}
