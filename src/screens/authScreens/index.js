import SignIn from "./SignInScreen";
import SignUp from './SignUpScreen'
import ForgotPassword from "./ForgotScreen";
import ChangePassword from "./ChangePasswordScreen";
import Otp from './OtpScreen'

export  {
    SignIn,
    SignUp,
    ForgotPassword,
    ChangePassword,
    Otp,
}



