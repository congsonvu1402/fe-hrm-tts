import ForgotPassword from "@/components/authComponents/ForgotPassword";

export default function Forgot(props) {
  return <ForgotPassword {...props} />;
}
