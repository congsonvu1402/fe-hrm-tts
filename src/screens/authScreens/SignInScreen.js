import React from "react";
import { View} from "react-native";
import SignInComponent from '../../components/authComponents/SignInComponent'

const SignIn = (props) => {
    return (
        <View>
            <SignInComponent {...props}/>
        </View>
    );
};


export default  SignIn;