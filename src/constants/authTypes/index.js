import * as signInTypes from './SignInType'
import * as SignUp from './SignUpTypes';
import * as NotifyType from './NotifyType';
import * as Forgot from './ForgotTypes';
import * as Confirm from './Confirm'
import * as ChangePass from './ChangePasswordType';
import * as CheckTokenTime from './CheckTokenTime';
import * as SignOutType from './SignOutType';
import * as OtpType from './OtpType'
export {
    signInTypes,
    SignUp,
    NotifyType,
    Forgot,
    Confirm,
    ChangePass,
    CheckTokenTime,
    SignOutType, 
    OtpType,
}