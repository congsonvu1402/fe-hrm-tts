import { all } from 'redux-saga/effects'
import * as authSaga from './authSaga'

const {ForgotPassSaga, SignUpSaga, SignInSaga, ChangePasswordSaga,OtpSaga} = authSaga

export default function* rootSaga() {
    yield all([
        ...ForgotPassSaga,
        ...SignInSaga,
        ...SignUpSaga,
        ...ChangePasswordSaga,
        ...OtpSaga
    ])
}