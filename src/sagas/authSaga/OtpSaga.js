import { takeLatest, put } from 'redux-saga/effects'
import { authActions } from '@/actions'
import { authTypes } from '@/constants'



const { OtpAction, NotifyAction } = authActions;

function* Otp(payload) {
    try {
        const otp = ['1', '2', '3', '4', '5', '6']
        let key = Object.values(payload.payload)
        const res = yield {
            notifyType: 'success',
            message: 'Đúng mã OTP',
        }
        if (JSON.stringify(otp) === JSON.stringify(key)) {
            yield put(OtpAction.otpSuccess({
                message: res.message,
            }))
        } else {
            throw {
                message: "Sai mã OTP"
            }
        }
    } catch (error) {
        yield put(OtpAction.otpFailure({
            message: error.message,
        }))
    }
}
function* ClearNotifications({ payload }) {
    try {
        if (payload.errorMessage) {
            yield put(NotifyAction.NotifySuccess({
                errorMessage: null,
            }))
        } else {
            const token = yield getStore('userToken')
            yield put(NotifyAction.NotifySuccess({
                message: null,
            }))
        }
    } catch (error) {
        yield put(NotifyAction.NotifyFailure({
            message: error.message,
        }))
    }
}


const OtpSaga = [
    takeLatest(authTypes.OtpType.OTP_REQUEST, Otp),
    takeLatest(authTypes.NotifyType.CLEAR_REQUEST, ClearNotifications),
]

export default OtpSaga;