import { takeLatest, put } from 'redux-saga/effects'
import { authActions } from '@/actions'
import { authTypes } from '@/constants'
const { ChangePassAction, NotifyAction } = authActions;

function* ChangePassword({ payload }) {
    try {
        const res = yield {
            notifyType: 'success',
            message: 'Doi mat khau thanh cong'
        }
        if (res.notifyType === 'success') {
            yield put(ChangePassAction.ChangePassSuccess({
                message: res.message,
            }))
        } else {
            throw {
                message: res.message
            }
        }
    } catch (error) {
        yield put(ChangePassAction.ChangePassFailure({
            message: error.message,
        }))
    }
}

function* ClearNotifications({ payload }) {
    try {
        if (payload.errorMessage) {
            yield put(NotifyAction.NotifySuccess({
                errorMessage: null,
            }))
        } else {
            yield put(NotifyAction.NotifySuccess({
                message: null,
            }))
        }
    } catch (error) {
        yield put(NotifyAction.NotifyFailure({
            message: error.message,
        }))
    }
}


const ChangePasswordSaga = [
    takeLatest(authTypes.ChangePass.CHANGE_PASSWORD_REQUEST, ChangePassword),
    takeLatest(authTypes.NotifyType.CLEAR_REQUEST, ClearNotifications),
]

export default ChangePasswordSaga;