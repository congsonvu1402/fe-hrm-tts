import { takeLatest, put } from 'redux-saga/effects'
import { authActions } from '@/actions'
import { authTypes } from '@/constants'

const { ForgotPassAction, NotifyAction } = authActions;

function* ForgotPass() {
    try {
        const res = yield {
            notifyType: 'success',
            message: 'Congratulations !!!!!'
        }
        if (res.notifyType === 'success') {
            yield put(ForgotPassAction.forgotPasswordSuccess({
                message: res.message,
            }))
        } else {
            throw {
                message: res.message
            }
        }
    } catch (error) {
        yield put(ForgotPassAction.forgotPasswordFailure({
            message: error.message,
        }))
    }
}

function* ClearNotifications() {
    try {
        yield put(NotifyAction.NotifySuccess({
            message: null,
            errorMessage: null,
        }))
    } catch (error) {
        yield put(NotifyAction.NotifyFailure({
            message: error.message,
        }))
    }
}


const ForgotPassSaga = [
    takeLatest(authTypes.Forgot.FORGOT_PASSWORD_REQUEST, ForgotPass),
    takeLatest(authTypes.NotifyType.CLEAR_REQUEST, ClearNotifications),
]

export default ForgotPassSaga;