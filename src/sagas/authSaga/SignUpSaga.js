import { takeLatest, put } from 'redux-saga/effects'
import { authActions } from '@/actions'
import { authTypes } from '@/constants'
import * as utils from '@/utils';
const { SignUpAction, NotifyAction } = authActions;
const { setStore } = utils.Storage;

function* SignUp({ payload }) {
    try {
        const res = yield {
            notifyType: 'success',
            message: 'Moi vao mail lay ma OTP'
        }
        yield setStore("Email", payload.email)
        if (res.notifyType === 'success') {
            yield put(SignUpAction.signUpSuccess({
                message: res.message,
            }))
        } else {
            throw {
                message: res.message
            }
        }
    } catch (error) {
        yield put(SignUpAction.signUpFailure({
            message: error.message,
        }))
    }
}

function* ClearNotifications({ payload }) {
    try {
        if (payload.errorMessage) {
            yield put(NotifyAction.NotifySuccess({
                errorMessage: null,
            }))
        } else {
            yield put(NotifyAction.NotifySuccess({
                message: null,
            }))
        }
    } catch (error) {
        yield put(NotifyAction.NotifyFailure({
            message: error.message,
        }))
    }
}

const SignUpSaga = [
    takeLatest(authTypes.SignUp.SIGN_UP_REQUEST, SignUp),
    takeLatest(authTypes.NotifyType.CLEAR_REQUEST, ClearNotifications),
]
export default SignUpSaga;