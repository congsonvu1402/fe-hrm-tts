import { takeLatest, put } from 'redux-saga/effects'
import { authActions } from '@/actions'
import * as utils from '@/utils';
import { authTypes } from '@/constants'

const { setStore, getStore } = utils.Storage
const { SignInAction, NotifyAction, CheckTokenTimeAction, SignOutAction } = authActions;

function* SignIn({ payload }) {
    try {
        const res = yield {
            notifyType: 'success',
            message: 'Moi vao mail lay ma OTP',
            token: 'This Is Token'
        }
        yield setStore("Email", payload.email)
        if (res.notifyType === 'success') {
            yield setStore('userToken', res.token)
            yield put(SignInAction.signInSuccess({
                message: res.message,
            }))
        } else {
            throw {
                message: res.message
            }
        }
    } catch (error) {
        yield put(SignInAction.signInFailure({
            message: error.message,
        }))
    }
}

function* ClearNotifications({ payload }) {
    try {
        if (payload.errorMessage) {
            yield put(NotifyAction.NotifySuccess({
                errorMessage: null,
            }))
        } else {
            const token = yield getStore('userToken')
            yield put(NotifyAction.NotifySuccess({
                message: null,
                userToken: token
            }))
        }
    } catch (error) {
        yield put(NotifyAction.NotifyFailure({
            message: error.message,
        }))
    }
}

function* CheckTokenTime({ payload }) {
    try {
        const res = yield {
            tokenType: true,
            message: 'moi ban dang nhap lai'
        }
        if (payload.token) {
            if (res.tokenType) {
                yield put(CheckTokenTimeAction.CheckTokenTimeSuccess({
                    token: payload.token,
                }))
            } else {
                yield removeStore('userToken')
                throw {
                    message: res.message
                }
            }
        } else {
            yield put(CheckTokenTimeAction.CheckTokenTimeSuccess({
                token: payload.token,
            }))
        }
    } catch (error) {
        yield put(CheckTokenTimeAction.CheckTokenTimeFailure({
            message: error.message,
        }))
    }
}

function* SignOut() {
    try {
            yield put(SignOutAction.SignOutSuccess({
                userToken: null
            }))
    } catch (error) {
        yield put(SignOutAction.SignOutFailure({
            message: error.message,
        }))
    }
}

const SignInSaga = [
    takeLatest(authTypes.signInTypes.SIGN_IN_REQUEST, SignIn),
    takeLatest(authTypes.NotifyType.CLEAR_REQUEST, ClearNotifications),
    takeLatest(authTypes.CheckTokenTime.TOKEN_TIME_REQUEST, CheckTokenTime),
    takeLatest(authTypes.SignOutType.SIGN_OUT_REQUEST, SignOut),
]

export default SignInSaga;